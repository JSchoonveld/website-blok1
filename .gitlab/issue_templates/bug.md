**Hotfix regarding issue**

**Fix**

**Checklist**
- [ ] Released
- [ ] Fix validated on production
- [ ] Release notes updated
- [ ] Merged master back into release/* and the develop branch

/label ~"bug"
