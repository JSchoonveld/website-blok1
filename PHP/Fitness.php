<?php

$images = [
    'barbell bench press' => '<img src="bbBench.jpg" alt="benchP">'
];

$routine = function (){
    $exercises = [
        'chest'=>['barbell bench press', 'dumbbell bench press', 'chest flies'],
        'back'=>['pull ups', 'barbell row', 'chin ups', 'seated row'],
        'shoulders'=>['lateral raises', 'barbell press', 'face pulls'],
        'triceps'=>['lat pushdowns', 'close grip bench press', 'skullcrushers'],
        'biceps'=>['dumbbell curls', 'hammer curls'],
        'quadriceps'=>['squat', 'leg press', 'hack squat'],
        'hamstrings'=>['good mornings','deadlift'],
        'calves' => ['seated calves', 'standing up calves']
    ];

    $images = [
        'barbell bench press' => '<img class="fitPic" src="img/exercises/bbBench.jpg" alt="benchP">',
        'dumbbell bench press' => '<img class="fitPic" src="img/exercises/dbBench.jpg" alt="dumbp">',
        'chest flies' => '<img class="fitPic" src="img/exercises/flies.jpg" alt="flies">',
        'chin ups' => '<img class="fitPic" src="img/exercises/chins.jpg" alt="chup">',
        'pull ups' => '<img class="fitPic" src="img/exercises/pullup.jpg" alt="pullup">',
        'barbell row' => '<img class="fitPic" src="img/exercises/barbellrow.jpg" alt="bbrow">',
        'seated row' => '<img class="fitPic" src="img/exercises/seatedrow.webp" alt="seatedRow">',
        'lateral raises' => '<img class="fitPic" src="img/exercises/lateral.jpg" alt="latraise">',
        'barbell press' => '<img class="fitPic" src="img/exercises/press.jpg" alt="press">',
        'face pulls' => '<img class="fitPic" src="img/exercises/facepull.jpg" alt="fpull">',
        'lat pushdowns' => '<img class="fitPic" src="img/exercises/pushdown.jpg" alt="lpush">',
        'close grip bench press' => '<img class="fitPic" src="img/exercises/closeg.jpg" alt="crbench">',
        'skullcrushers' => '<img class="fitPic" src="img/exercises/skullcrusher.jpg" alt="skullcr">',
        'dumbbell curls' => '<img class="fitPic" src="img/exercises/curls.jpg" alt="dcurl">',
        'hammer curls' => '<img class="fitPic" src="img/exercises/hcurls.jpg" alt="hcurl">',
        'squat' => '<img class="fitPic" src="img/exercises/Squat.jpg" alt="squat">',
        'leg press' => '<img class="fitPic" src="img/exercises/lpress.jpg" alt="lpress">',
        'hack squat' => '<img class="fitPic" src="img/exercises/hsquat.jpg" alt="hsquat">',
        'good mornings' => '<img class="fitPic" src="img/exercises/goodMorning.jpg" alt="gmorning">',
        'deadlift' => '<img class="fitPic" src="img/exercises/deadlift.jpg" alt="dlift">',
        'seated calves' => '<img class="fitPic" src="img/exercises/scalves.png" alt="secalves">',
        'standing up calves' => '<img class="fitPic" src="img/exercises/stcalves.jpg" alt="stcalves">'

    ];

    foreach ($exercises as $exercise){
        echo '<div class="fitPic-container">';
        echo '<p><b>' . $exercise[array_rand($exercise)] . ' ' . random_int(3,5) .' sets</b></p>';
            echo $images[$exercise[array_rand($exercise)]];
        echo '</div>';
    }
};
