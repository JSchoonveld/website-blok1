var button = document.getElementsByClassName('buttons-section')[0];
var g1 = document.getElementsByClassName('g1')[0];
var g2 = document.getElementsByClassName('g2')[0];
var plus = document.getElementById('add');
var sub = document.getElementById('subtract');
var res = document.getElementById('result');

var value1 = null;
var value2 = null;
var operation = null;

button.addEventListener('click', function (evt){

    if(evt.target.value === 'clr'){
        value1 = null;
        value2 = null;
        g1.innerHTML = 0;
        g2.innerHTML = 0;
    }

    if(evt.target.value === '=' ){
        value2 = parseFloat(g1.innerHTML);
        var result = new Calculator(value1, value2);
        console.log(result.calc(operation));
        g1.innerHTML = result.calc(operation);
        g2.innerHTML = 0;

    }


    if(g1.innerHTML === '0' && value1 === null && evt.target.value !== 'clr'){
        g1.innerHTML = evt.target.value;
    }
    else if(g1.innerHTML !== '0' && value1 === null){
        if (evt.target.classList.contains('operand')){
            value1 = parseFloat(g1.innerHTML);
            operation = evt.target.value;
            g1.innerHTML = '0';
            g2.innerHTML = value1 + evt.target.value;
        }
        else {
            g1.innerHTML += evt.target.value;
        }

    }
    else if(g1.innerHTML === '0'&& value1 !== null){
        console.log('test');
        g1.innerHTML = evt.target.value;
    }
    else if(g1.innerHTML !== '0'&& value1 !== null){
        console.log('yo');
        g1.innerHTML += evt.target.value;
    }


})

function Calculator(g1,g2){
    this.g1 = g1;
    this.g2 = g2;
    this.calc = function (operand){
        if(operand === '+'){
            return g1 + g2;
        }
        else if(operand === '-'){
            return g1 - g2;
        }
        else if(operand === '*'){
            return g1 * g2;
        }
        else if(operand === '/'){
            return g1 / g2;
        }
    }
}


let requestURL = `http://api.openweathermap.org/data/2.5/weather?q=Groningen&units=metric&lang=nl&appid=98ab55601531fa7473634a60a5437be7`;


let request = new XMLHttpRequest();

request.open('GET', requestURL, true);

request.responseType = 'json';

request.send();


request.onload = () => {
    let data = request.response;

    addData(data);
}


let addData = (jsonData) => {

        let show = document.getElementsByClassName('weather')[0];

        let weather = jsonData.weather;
        let main = jsonData.main

        let div = document.createElement('div');
        div.innerHTML = `<h3>Het weer op dit moment in Groningen</h3>`
        div.innerHTML += '<br>' + weather[0].description + '<br><br>';
        if(weather[0].description === 'lichte regen' || weather[0].description === 'zware regen' || weather[0].description === 'lichte motregen' || weather[0].description === 'motregen' || weather[0].description === 'matige regen') {

            div.innerHTML += '<img src="img/weather/light_rain.png" alt="rain"><br><br>';
        }
        else if(weather[0].description === 'half bewolkt' || weather[0].description === 'bewolkt'){
            div.innerHTML += '<img src="img/weather/cloudy.png" alt="bewolkt"><br><br>';
        }
        else if(weather[0].description === 'zeer lichte bewolking' || weather[0].description === 'licht bewolkt'){
            div.innerHTML += '<img src="img/weather/cloudyAndSunny.png" alt="halfBewolkt"><br><br>';
        }
        else if(weather[0].description === 'onbewolkt'){
            div.innerHTML += '<img src="img/weather/sunny.png" alt="sunny"><br><br>';
        }
        div.innerHTML += 'Temperatuur: ' + main.temp + ' graden Celsius<br>';
        div.innerHTML += 'Gevoelstemperatuur: ' + main.feels_like + ' graden Celsius<br>';
        show.appendChild(div);

}
